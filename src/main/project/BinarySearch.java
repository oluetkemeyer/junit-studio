package project;

public class BinarySearch {

    /**
     * A binary search implementation for integer arrays.
     *
     * Info about binary search: https://www.geeksforgeeks.org/binary-search/
     *
     * @param sortedNumbers - must be sorted from least to greatest
     * @param n - number to search for
     * @return index of search item if it's found, -1 if not found
     */
    public static int binarySearch(int[] sortedNumbers, int n) {
        double right = (double) sortedNumbers.length - 1;
        double left = 0;
        while (right >= left) {
            int mid = (int) Math.round(left + ((right - left) / 2));
            if (sortedNumbers[mid] > n) {
                right = mid;
            } else if (sortedNumbers[mid] < n && right != left) {
                left = mid;
            } else if (right == left && sortedNumbers[mid] != n){
                return -1;
            } else {
                return mid;
            }
        }
        return -1;
    }

}
