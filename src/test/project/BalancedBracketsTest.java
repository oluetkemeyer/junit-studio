package project;

import org.junit.Test;

import static org.junit.Assert.*;


public class BalancedBracketsTest {

    //test if true

    @Test
    public void testBracketParseTrue(){
        String testString = "This is a [balanced] bracket";
        boolean result = BalancedBrackets.hasBalancedBrackets(testString);
        assertEquals(true, result);
    }

    @Test
    public void testBracketParseFalse(){
        String testString = "This is an [unbalanced bracket";
        boolean result = BalancedBrackets.hasBalancedBrackets(testString);
        assertEquals(false, result);
    }

    @Test
    public void testNestedBracketsTrue(){
        String testString = "This is [a [nested] bracket]";
        boolean result = BalancedBrackets.hasBalancedBrackets(testString);
        assertEquals(true, result);
    }

    @Test
    public void testNestedBracketsFalse(){
        String testString = "This is [a [nested bracket]";
        boolean result = BalancedBrackets.hasBalancedBrackets(testString);
        assertEquals(false, result);
    }

    @Test
    public void testTrivialCase(){
        String testString = "This case is trivial.";
        boolean result = BalancedBrackets.hasBalancedBrackets(testString);
        assertEquals(true, result);
    }

    @Test
    public void testMismatch(){
        String testString = "This bracket is ]mismatched[";
        boolean result = BalancedBrackets.hasBalancedBrackets(testString);
        assertEquals(false, result);
    }

}
