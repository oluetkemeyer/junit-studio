package project;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class BinarySearchTest {
    int testArray[] = new int[3];
    @Before
    public void initialize(){
        for (int i = 1; i <= 3; i++){
            testArray[i - 1] = i;
        }
    }

    @Test
    public void testSimpleArray(){
        int n = BinarySearch.binarySearch(testArray, 2);
        assertEquals(1, n, .001);
    }

    @Test
    public void testMissingNumberResponse(){
        int n = BinarySearch.binarySearch(testArray, 4);
        assertEquals(-1, n, .001);
    }

    @Test
    public void testHardArray(){
        int hardArray[] = new int[]{12, 18, 76, 79, 101, 128, 166, 455, 1000, 715431846};
        int n = BinarySearch.binarySearch(hardArray, 76);
        assertEquals(2, n, .001);
    }
}
