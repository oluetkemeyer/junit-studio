package project;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FractionTest {
    Fraction testFraction = new Fraction(1,2);
    @Before
    public void init(){
        testFraction.setDenominator(2);
        testFraction.setNumerator(1);
    }

    @Test
   public void testFractionAddition(){
        Fraction result = testFraction.add(new Fraction(1,4));
        assertEquals(3, result.getNumerator(), .001);
        assertEquals(4, result.getDenominator(), .001);
   }

   @Test
    public void testIntegerAddition(){
        Fraction result = testFraction.add(new Integer(2));
        assertEquals(5, result.getNumerator(), .001);
        assertEquals(2, result.getDenominator(), .001);
   }

   @Test
    public void testString(){
        String result = testFraction.toString();
        assertEquals("1/2", result);
   }

   @Test
    public void testEquals(){
        boolean result1 = testFraction.equals(new Fraction(2,4));
        boolean result2 = testFraction.equals(new Fraction(1,3));
        assertEquals(true, result1);
        assertEquals(false, result2);
   }

   @Test
    public void testCompareTo(){
        Fraction newFraction = new Fraction (1,3);
        int result = testFraction.compareTo(newFraction);
        boolean compare = result > 0;
        assertEquals(true, compare);
   }

   @Test
    public void testNegativeVals(){
        Fraction newFraction = new Fraction (-1, 3);
        Fraction result = newFraction.add(new Fraction (1,2));
        assertEquals(1, result.getNumerator(),.001);
        assertEquals(6, result.getDenominator(), .001);
   }

}
