package project;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RomanNumeralTest {

    @Test
    public void testFive(){
        String result = RomanNumeral.fromInt(5);
        assertEquals("V", result);
    }

    @Test
    public void testFour(){
        String result = RomanNumeral.fromInt(4);
        assertEquals("IV", result);
    }

    @Test
    public void testBigNumber(){
        String result = RomanNumeral.fromInt(2440);
        assertEquals("MMCDXL", result);
    }
}
